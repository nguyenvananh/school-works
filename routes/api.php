<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('signup', ['as' => 'singup', 'use' => 'Api\AuthController@register']); 
Route::post('login',  ['as' => 'login', 'use' => 'Api\AuthController@login']);

Route::group(['prefix' => 'auth','middleware' => 'jwt.auth','name' => 'auth.'], function () { 
	Route::get('user', ['as' => 'user', 'use' => 'Api\AuthController@user']); 
	Route::post('logout', ['as' => 'logout', 'use' => 'Api\AuthController@logout']); 
});

Route::middleware('jwt.refresh')->get('/token/refresh', ['as' => 'refresh', 'use' => 'Api\AuthController@refresh']);