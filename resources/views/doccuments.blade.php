<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Under Construction RESTful APL</title>
	<base href="{{ asset('') }}">
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	<h1 class="text-center">Hello! Under Construction RESTful API</h1>
	<h3 class="text-center">This is page show Doccuments from API</h3>
	
	<div class="container">
		<table class="table table-hover table-responsive table-bordered">
			<thead>
				<tr>
					<th class="text-center">STT</th>
					<th class="text-center">Group</th>
					<th class="text-center">Name</th>
					<th class="text-center">Route</th>
					<th class="text-center">Methods</th>
					<th class="text-center">Description</th>
					<th class="text-center">Example</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>